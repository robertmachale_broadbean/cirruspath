<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request)
    {
        // replace this example code with whatever you need
        return $this->render('default/index.html.twig', array(
            'base_dir' => realpath($this->container->getParameter('kernel.root_dir').'/..').DIRECTORY_SEPARATOR,
        ));
    }

    /**
     * @Route("/tasks", name="tasksGet")
     * @Method("GET")
     */
    public function tasksGet(Request $request)
    {
        $instance_url = "https://robert-machale-dev-ed.my.salesforce.com";
        $access_token = "00Df4000000208C!AQgAQGkOTt91wfYNNZtMaHmluMCCvqop5d8ncAhQ.AyCyJdxS.0REbqRWR3L.anKTxswey4YJ3Mr7yUgUpRfWeQywaQ.pFBv";

        $query = "SELECT Id,Subject from Task LIMIT 100";
        $url = "$instance_url/services/data/v20.0/query?q=" . urlencode($query);

        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HTTPHEADER,
            array("Authorization: OAuth $access_token"));

        $json_response = curl_exec($curl);
        curl_close($curl);

        $response = new Response();
        $response->headers->set('Content-Type', 'text/json');
        $response->setContent($json_response);

        return $response;

    }

    /**
     * @Route("/task/{id}", name="taskGet")
     * @Method("GET")
     */
    public function taskGet(Request $request, $id)
    {
        $instance_url = "https://robert-machale-dev-ed.my.salesforce.com";
        $access_token = "00Df4000000208C!AQgAQGkOTt91wfYNNZtMaHmluMCCvqop5d8ncAhQ.AyCyJdxS.0REbqRWR3L.anKTxswey4YJ3Mr7yUgUpRfWeQywaQ.pFBv";

        //$id = '00Tf40000023gm4EAA';
        $url = "$instance_url/services/data/v20.0/sobjects/Task/$id";

        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HTTPHEADER,
            array("Authorization: OAuth $access_token"));

        $json_response = curl_exec($curl);
        curl_close($curl);

        $response = new Response();
        $response->headers->set('Content-Type', 'text/json');
        $response->setContent($json_response);

        return $response;

    }

    /**
     * @Route("/task/{id}", name="taskPatch")
     * @Method("PATCH")
     */
    public function taskPatch(Request $request, $id)
    {
        $instance_url = "https://robert-machale-dev-ed.my.salesforce.com";
        $access_token = "00Df4000000208C!AQgAQGkOTt91wfYNNZtMaHmluMCCvqop5d8ncAhQ.AyCyJdxS.0REbqRWR3L.anKTxswey4YJ3Mr7yUgUpRfWeQywaQ.pFBv";

        //$id = '00Tf40000023gm4EAA';
        $url = "$instance_url/services/data/v20.0/sobjects/Task/$id";

        //$content = json_encode(array("subject" => $new_subject,));
        $content = $request->getContent();

        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt($curl, CURLOPT_HTTPHEADER,
            array("Authorization: OAuth $access_token",
                "Content-type: application/json"));
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "PATCH");
        curl_setopt($curl, CURLOPT_POSTFIELDS, $content);

        curl_exec($curl);
        $json_response = "";
        $status = curl_getinfo($curl, CURLINFO_HTTP_CODE);

        if ( $status != 204 ) {
            die("Error: call to URL $url failed with status $status, curl_error " . curl_error($curl) . ", curl_errno " . curl_errno($curl));
        }

        curl_close($curl);

        $response = new Response();
        $response->headers->set('Content-Type', 'text/json');
        $response->setContent($json_response);

        return $response;

    }

    /**
     * @Route("/task", name="taskPost")
     * @Method("POST")
     */
    public function taskPost(Request $request)
    {
        $instance_url = "https://robert-machale-dev-ed.my.salesforce.com";
        $access_token = "00Df4000000208C!AQgAQGkOTt91wfYNNZtMaHmluMCCvqop5d8ncAhQ.AyCyJdxS.0REbqRWR3L.anKTxswey4YJ3Mr7yUgUpRfWeQywaQ.pFBv";

        //$id = '00Tf40000023gm4EAA';
        $url = "$instance_url/services/data/v20.0/sobjects/Task/";

        //$content = json_encode(array("Subject" => $subject));
        $content = $request->getContent();

        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HTTPHEADER,
            array("Authorization: OAuth $access_token",
                "Content-type: application/json"));
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $content);

        $json_response = curl_exec($curl);
        $json_response = "";
        $status = curl_getinfo($curl, CURLINFO_HTTP_CODE);

        if ( $status != 201 ) {
            die("Error: call to URL $url failed with status $status, curl_error " . curl_error($curl) . ", curl_errno " . curl_errno($curl));
        }

        curl_close($curl);

        $response = new Response();
        $response->headers->set('Content-Type', 'text/json');
        $response->setContent($json_response);

        return $response;

    }

    /**
     * @Route("/task/{id}", name="taskPatch")
     * @Method("DELETE")
     */
    public function taskDelete(Request $request, $id)
    {
        $instance_url = "https://robert-machale-dev-ed.my.salesforce.com";
        $access_token = "00Df4000000208C!AQgAQGkOTt91wfYNNZtMaHmluMCCvqop5d8ncAhQ.AyCyJdxS.0REbqRWR3L.anKTxswey4YJ3Mr7yUgUpRfWeQywaQ.pFBv";

        //$id = '00Tf40000023gm4EAA';
        $url = "$instance_url/services/data/v20.0/sobjects/Task/$id";

        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt($curl, CURLOPT_HTTPHEADER,
            array("Authorization: OAuth $access_token"));
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "DELETE");

        curl_exec($curl);
        $json_response = "";
        $status = curl_getinfo($curl, CURLINFO_HTTP_CODE);

        if ( $status != 204 ) {
            die("Error: call to URL $url failed with status $status, curl_error " . curl_error($curl) . ", curl_errno " . curl_errno($curl));
        }

        curl_close($curl);

        $response = new Response();
        $response->headers->set('Content-Type', 'text/json');
        $response->setContent($json_response);

        return $response;

    }

}
